import { NgModule } from "@angular/core";
import { Test2Component } from "./test-component2.component";

@NgModule({
    declarations: [
      Test2Component
    ],
    imports: [
        // PippoModule
    ],
    exports: [
      Test2Component
    ],
    providers: [],
    bootstrap: []
  })
  export class Test2Module { }
  