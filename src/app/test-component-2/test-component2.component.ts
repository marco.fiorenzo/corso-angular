import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GenericService } from '../service/generic.service';

@Component({
  selector: 'app-test2-component',
  templateUrl: './test-component2.component.html',
  styleUrls: ['./test-component2.component.scss']
})
export class Test2Component implements OnInit {

  @Input('text') text: string | undefined;
  @Output('result') result: EventEmitter<string> = new EventEmitter();

  constructor(private genericService: GenericService) { }

  ngOnInit(): void {
  }

  emitEvent() {
    this.result.emit(this.text + ' ciao');
    this.genericService.evento.emit(this.text + ' ciao');
    console.log(this.genericService.pippo);
  }

}
