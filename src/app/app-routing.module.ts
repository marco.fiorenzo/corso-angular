import { RouterModule, Routes } from "@angular/router";
import { TestComponent } from "./test-component/test-component.component";
import { NgModule } from "@angular/core";
import { ComponenteLontanoComponent } from "./componente-lontano/componente-lontano.component";


const routes: Routes = [
    {
        path: 'test',
        component: TestComponent
    },
    {
        path: 'componenteLontano/:variabile',
        component: ComponenteLontanoComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }