import { Component, OnInit } from '@angular/core';
import { GenericService } from '../service/generic.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-componente-lontano',
  templateUrl: './componente-lontano.component.html',
  styleUrls: ['./componente-lontano.component.scss']
})
export class ComponenteLontanoComponent implements OnInit {

  eventoEmesso: string;
  variabileDiPatht: string;

  formUtente: FormGroup;

  constructor(private genericService: GenericService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder) {
      this.formUtente = fb.group({
        nomeUtente: new FormControl('', [Validators.required, Validators.minLength(5), Validators.email]),
        password: new FormControl('', [Validators.required, Validators.minLength(5)])
      });
     }

  ngOnInit(): void {
    this.genericService.evento.subscribe(pippo => {
      this.eventoEmesso = pippo;
      this.genericService.pippo = 'PLUTO';
    });
    this.variabileDiPatht = this.activatedRoute.snapshot.params['variabile'];
  }

  verifica() {
    console.log("debug");
  }

  onSubmit() {
    let nomeUtente = this.formUtente.controls['nomeUtente'].value;
    let password = this.formUtente.controls['password'].value;

    alert('Nome Utente: ' + nomeUtente + ' Password: ' + password);
  }

  controlla() {
    let nomeUtente = this.formUtente.controls['nomeUtente'].value;
    if(nomeUtente.includes('gmail')) {
      this.formUtente.setErrors({ 'customErrors': true});
    } else {
      this.formUtente.setErrors(null);
    }
  }

}
