import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TestComponent } from './test-component/test-component.component';
import { AppRoutingModule } from './app-routing.module';
import { TestModule } from './test-component/test.module';
import { Test2Module } from './test-component-2/test2.module';
import { ComponenteLontanoComponent } from './componente-lontano/componente-lontano.component';
import { GenericService } from './service/generic.service';
import {  ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ComponenteLontanoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TestModule,
    Test2Module,
    ReactiveFormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
