import { NgModule } from "@angular/core";
import { TestComponent } from "./test-component.component";
import { Test2Module } from "../test-component-2/test2.module";
import { BrowserModule } from "@angular/platform-browser";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

@NgModule({
    declarations: [
      TestComponent
    ],
    imports: [
        Test2Module,
        BrowserModule,
        CommonModule,
        FormsModule
    ],
    exports: [],
    providers: [],
    bootstrap: []
  })
  export class TestModule { }
  