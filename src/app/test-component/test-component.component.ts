import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test-component',
  templateUrl: './test-component.component.html',
  styleUrls: ['./test-component.component.scss']
})
export class TestComponent implements OnInit {

  text: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  generateRandom() {
    this.text = this.text + 'x';
  }

  manageEvent($event: string){
    this.text = $event;
  }

}
