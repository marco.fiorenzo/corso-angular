import { EventEmitter, Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class GenericService {

    public evento: EventEmitter<any> = new EventEmitter();
    public pippo: string = 'PIPPO';

}